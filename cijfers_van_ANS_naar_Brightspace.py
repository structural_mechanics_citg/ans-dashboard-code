"""
Downloads results from ANS and exports them to a csv file for Brightspace where the normal export function does not work because of ungraded open questions.
Only the highest grade per student is exported
A filterdate can be set to only download results after a certain date
The Bearer code is personal and should be replaced by your own code
"""
#%% 
import requests
from tqdm import tqdm #conda install tqdm
import pandas as pd
from datetime import datetime
import pytz
now = datetime.now(pytz.timezone('Europe/Amsterdam'))
dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
print("Laatste update: ", dt_string)

#%% authenticatioe
header = {"accept": "application/json", "Authorization": "Bearer ###"}

#%% Download assignment list en filter op COZ opgaves
course_code = "236476" #290836 voor TOZ, 236476 voor COZ, "329111" voor CM2
assignmentsresponse = requests.get("https://ans.app/api/v2/courses/"+course_code+"/assignments?limit=100&page=1",headers=header)
assignmentsjson = assignmentsresponse.json()
assignmentsid = []
name = []
for i in range(len(assignmentsjson)):
    if 'COZ' in assignmentsjson[i]['name']:
        assignmentsid.append(assignmentsjson[i]['id'])
        name.append(assignmentsjson[i]["name"])

#%% download resultaten COZ blokken en filter op relevante datea
resultassignments = []
results_user_id = []
comments = []
date_format = '%Y-%m-%dT%H:%M:%S.%f%z'
filterdate = datetime.strptime('2023-11-13T00:00:00.000+02:00',date_format)
#filterdate = datetime.strptime('2023-08-13T00:00:00.000+02:00',date_format)

for j in tqdm(range(len(assignmentsid)),desc="Download result per blok"):
    idofnewlist = ""
    results_user_id_temp = []
    assignments_id = assignmentsid[j]
    result_id = []
    user_id = []
    succeeded = []
    graded = []
    points = []
    mark = []
    resultassignmentstemp = dict()
    for k in range(10): #max 1000 results
        resultsresponse = requests.get("https://ans.app/api/v2/assignments/"+str(assignmentsid[j])+"/results?limit=100&page="+str(k+1),headers=header)
        resultsjson = resultsresponse.json()
        if resultsjson==[] or resultsjson[0]['id'] == idofnewlist:
            break
        else:
            idofnewlist = resultsjson[0]['id']
        maximum_points_stored = False
        for i in range(len(resultsjson)):
            if resultsjson[i]["submitted_at"]:
                if datetime.strptime(resultsjson[i]["submitted_at"],date_format) < filterdate:
                    continue
                result_id.append(resultsjson[i]["id"])
                user_id.append(resultsjson[i]["user_id"])
                succeeded.append(1)
                if resultsjson[i]["mark"]:
                    graded.append(1)
                    mark.append(float(resultsjson[i]["mark"]))
                else:
                    graded.append(0)
                    mark.append(None)
                if resultsjson[i]["total_points"] != None:
                    points.append(float(resultsjson[i]["total_points"]))
                else:
                    points.append(1.0) #BUG in API
                    print(resultsjson[i]["id"])
                    print('Something is very wrong here, reported at ANS')
                if not maximum_points_stored:
                    resultassignmentstemp['total_points'] = float(resultsjson[i]['maximum_points'])
                    maximum_points_stored = True
                comment = resultsjson[i]["comment"]
                if comment != None:
                    commenttemp = dict()
                    commenttemp['assignment_id'] = assignments_id
                    commenttemp['result_id'] =  resultsjson[i]["id"]
                    commenttemp['comment'] = comment
                    comments.append(commenttemp) 
    if len(result_id)>0:
        resultassignmentstemp['assignments_id'] = assignments_id
        resultassignmentstemp['name'] = name[j]
        resultassignmentstemp['result_id'] = result_id
        resultassignmentstemp['succeeded'] = succeeded
        resultassignmentstemp['graded'] = graded
        resultassignmentstemp['points'] = points
        resultassignmentstemp['mark'] = mark
        resultassignments.append(resultassignmentstemp)
        results_user_id_temp= user_id
        results_user_id.append(results_user_id_temp)

for i in range(len(resultassignments)):
    resultassignments[i]['calculated_grade'] = []
    for j in range(len(resultassignments[i]['points'])):
        resultassignments[i]['calculated_grade'].append(min(round(resultassignments[i]['points'][j]/resultassignments[i]['total_points']*10+0.0000001,1),10))

#%% Maak lijst van unique studentennummers en voeg deze toe aan resultatenlijst
unique_user_id=[]
for i in range(len(results_user_id)):
    for j in range(len(results_user_id[i])):
        if results_user_id[i][j] not in unique_user_id:
            unique_user_id.append(results_user_id[i][j])

unique_student_id=[]
for i in tqdm(range(len(unique_user_id)),desc="Download student numbers"):
    usersresponse = requests.get("https://ans.app/api/v2/users/"+str(unique_user_id[i]),headers=header)
    if usersresponse.status_code != 403:
        usersresponsejson = usersresponse.json()
        unique_student_id.append(int(usersresponsejson["student_number"]))
    else:
        unique_student_id.append("Medewerker")

for i in range(len(resultassignments)):
    student_id = []
    for j in range(len(resultassignments[i]["result_id"])):
        student_id.append(unique_student_id[unique_user_id.index(results_user_id[i][j])])
    resultassignments[i]['student_id']= student_id

# %% Filter student_id en calculated grade op basis van hoogste cijfer
def find_duplicate_indices(lst):
    duplicate_indices = {}
    for index, item in enumerate(lst):
        if item in duplicate_indices:
            duplicate_indices[item].append(index)
        else:
            duplicate_indices[item] = [index]
    
    unique_duplicates = {item: indices for item, indices in duplicate_indices.items() if len(indices) > 1}
    return unique_duplicates

import copy
resultassignments_red = copy.deepcopy(resultassignments)
for i in range(len(resultassignments)):
    duplicates = find_duplicate_indices(resultassignments[i]['student_id'])
    print(duplicates)
    if len(duplicates)>0:
        minresult = []
        for key, value in duplicates.items():
            result0 = resultassignments[i]["calculated_grade"][value[0]]
            result1 = resultassignments[i]["calculated_grade"][value[1]]
            print(result0, result1)
            if result0 < result1:
                minresult.append(value[0])
            else:
                minresult.append(value[1])
        print(minresult)
        [resultassignments_red[i]["student_id"].pop(index) for index in sorted(minresult, reverse=True)]
        [resultassignments_red[i]["calculated_grade"].pop(index) for index in sorted(minresult, reverse=True)]


# %% select blok
Kies_blok_voor_vragen=resultassignments[14]["name"]
for i in range(len(name)):
    if Kies_blok_voor_vragen == resultassignments[i]["name"]:
        index_blok = i
        break
    

# %% exportfile voor Brightspace
import os
rows = [['OrgDefinedId',resultassignments_red[index_blok]["name"]+" Points Grade <Numeric MaxPoints:10 Category:COZ>","End-of-Line Indicator"]]
   
    
for i in range(len(resultassignments_red[index_blok]["calculated_grade"])):
    rows.append(["#"+str(resultassignments_red[index_blok]["student_id"][i]),resultassignments_red[index_blok]["calculated_grade"][i],"#"])
 
np.savetxt(os.path.join(os.getcwd()+"\\"+"Brightspace_output "+resultassignments_red[index_blok]["name"][0:6]+" "+datetime.now().strftime("%d-%m-%Y_%H-%M-%S")+".csv"),
        rows,
        delimiter =",",
        fmt ='% s')
    
# %%
