# -*- coding: utf-8 -*-
"""
Created on Wed Nov 15 14:22:53 2023

@author: tomvanwoudenbe
"""

import requests
from tqdm import tqdm #conda install tqdm
import pandas as pd
from datetime import datetime
import pytz
now = datetime.now(pytz.timezone('Europe/Amsterdam'))
dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
print("Laatste update: ", dt_string)

#%% authenticatioe
header = {"accept": "application/json", "Authorization": "Bearer ##vul hier je bearer code in##"}

#%% Download assignment list en filter op COZ opgaves
course_code = "## vul hier je ANS course code in ##"
assignmentsresponse = requests.get("https://ans.app/api/v2/courses/"+course_code+"/assignments?items=100&page=1",headers=header)
assignmentsjson = assignmentsresponse.json()
assignmentsid = []
name = []
for i in range(len(assignmentsjson)):
    assignmentsid.append(assignmentsjson[i]['id'])
    name.append(assignmentsjson[i]["name"])

#%% download resultaten COZ blokken en filter op relevante datea
resultassignments = []
results_user_id = []
comments = []
for j in tqdm(range(len(assignmentsid)),desc="Download result per blok"):
    idofnewlist = ""
    results_user_id_temp = []
    assignments_id = assignmentsid[j]
    result_id = []
    user_id = []
    succeeded = []
    graded = []
    points = []
    mark = []
    resultassignmentstemp = dict()
    for k in range(10): #max 1000 results
        resultsresponse = requests.get("https://ans.app/api/v2/assignments/"+str(assignmentsid[j])+"/results?items=100&page="+str(k+1),headers=header)
        resultsjson = resultsresponse.json()
        if resultsjson==[] or resultsjson[0]['id'] == idofnewlist:
            break
        else:
            idofnewlist = resultsjson[0]['id']
        for i in range(len(resultsjson)):
            if resultsjson[i]["submitted_at"]:
                result_id.append(resultsjson[i]["id"])
                user_id.append(resultsjson[i]["user_id"])
                succeeded.append(1)
                if resultsjson[i]["mark"]:
                    graded.append(1)
                    mark.append(float(resultsjson[i]["mark"]))
                else:
                    graded.append(0)
                    mark.append(None)
                points.append(float(resultsjson[i]["total_points"]))
                comment = resultsjson[i]["comment"]
                if comment != None:
                    commenttemp = dict()
                    commenttemp['assignment_id'] = assignments_id
                    commenttemp['result_id'] =  resultsjson[i]["id"]
                    commenttemp['comment'] = comment
                    comments.append(commenttemp) 
    if len(result_id)>0:
        resultassignmentstemp['assignments_id'] = assignments_id
        resultassignmentstemp['name'] = name[j]
        resultassignmentstemp['result_id'] = result_id
        resultassignmentstemp['succeeded'] = succeeded
        resultassignmentstemp['graded'] = graded
        resultassignmentstemp['points'] = points
        resultassignmentstemp['mark'] = mark
        resultassignments.append(resultassignmentstemp)
        results_user_id_temp= user_id
        results_user_id.append(results_user_id_temp)

for i in range(len(resultassignments)):
    for j in range(len(resultassignments[i]["mark"])):
        if resultassignments[i]["mark"][j] == 10.:
            resultassignments[i]['total_points'] = resultassignments[i]["points"][j]
            #print(resultassignments[i]['total points'])
            break
        if j == len(resultassignments[i]["mark"]) - 1:
            print('no maximum score found, temporarily highest score used')
            resultassignments[i]['total_points'] = round(max(resultassignments[i]["points"])/max(filter(None,resultassignments[i]["mark"]))*10,0)

for i in range(len(resultassignments)):
    resultassignments[i]['calculated_grade'] = []
    for j in range(len(resultassignments[i]['points'])):
        resultassignments[i]['calculated_grade'].append(min(round(resultassignments[i]['points'][j]/resultassignments[i]['total_points']*10+0.0000001,1),10))

#%% Maak lijst van unique studentennummers en voeg deze toe aan resultatenlijst
unique_user_id=[]
for i in range(len(results_user_id)):
    for j in range(len(results_user_id[i])):
        if results_user_id[i][j] not in unique_user_id:
            unique_user_id.append(results_user_id[i][j])

unique_student_id=[]
for i in tqdm(range(len(unique_user_id)),desc="Download student numbers"):
    usersresponse = requests.get("https://ans.app/api/v2/users/"+str(unique_user_id[i]),headers=header)
    if usersresponse.status_code != 403:
        usersresponsejson = usersresponse.json()
        unique_student_id.append(int(usersresponsejson["student_number"]))
    else:
        unique_student_id.append("Medewerker")

for i in range(len(resultassignments)):
    student_id = []
    for j in range(len(resultassignments[i]["result_id"])):
        student_id.append(unique_student_id[unique_user_id.index(results_user_id[i][j])])
    resultassignments[i]['student_id']= student_id

#%% Maak lijst van aantal behaalde en nagekeken blokken per student
succeeded = [0]*len(unique_student_id)
graded = [0]*len(unique_student_id)
for i in range(len(resultassignments)):
    student_id_temp = []
    graded_temp = [0]*len(unique_student_id)
    for j in range(len(resultassignments[i]['student_id'])):
        if resultassignments[i]['student_id'][j] in student_id_temp:
            index = unique_student_id.index(resultassignments[i]['student_id'][j])
            graded_temp[index] = max(resultassignments[i]['graded'][j],graded_temp[index])
        else:
            index = unique_student_id.index(resultassignments[i]['student_id'][j])
            succeeded[index] += 1
            graded_temp[index] += resultassignments[i]['graded'][j]
            student_id_temp.append(resultassignments[i]['student_id'][j])
    graded = [sum(x) for x in zip(graded, graded_temp)]


# %% Resultaten totaal
result_total = pd.DataFrame(list(zip(unique_student_id,succeeded,graded)),columns=["Student nummer","Aantal blokken gemaakt","Aantal blokken nagekeken"])
    
# %% opmerkingen
for i in range(len(comments)):
    print('Assignment_id =',comments[i]['assignment_id'],', Result_id =',comments[i]['result_id'],':',comments[i]['comment'][5:-6])

# %% Results per questions from insights
# questions = []
# for i in tqdm(range(len(resultassignments)),desc="Download insights per blok"):
#     questions_temp = dict()
#     exercise_id = []
#     exercise_name = []
#     question_id = []
#     question_position = []
#     insights_p_value = []
#     insights_rir_value = []
#     insights_updated = []
#     points = 0.0
#     exercisesresponse = requests.get("https://ans.app/api/v2/assignments/"+str(resultassignments[i]['assignments_id'])+"/exercises?items=100&page=1",headers=header)
#     exercisesjson = exercisesresponse.json()
#     for j in range(len(exercisesjson)):
#         exercise_id.append(exercisesjson[j]['id'])
#         exercise_name.append(exercisesjson[j]['name'])
#         questionsresponse = requests.get("https://ans.app/api/v2/exercises/"+str(exercisesjson[j]['id'])+"/questions?items=100&page=1",headers=header)
#         questionsjson = questionsresponse.json()
#         for k in range(len(questionsjson)):
#             question_id.append(questionsjson[k]["id"])
#             points += float(questionsjson[k]["points"]) #hulpvragen worden nu nog meegerekend
#             question_position.append(str(j+1)+chr(questionsjson[k]["position"]+ord('a')-1))
#             insightsresponse = requests.get("https://ans.app/api/v2/insights/questions/"+str(questionsjson[k]["id"]),headers=header)
#             if insightsresponse.status_code == 204:
#                 insights_p_value.append(float('nan'))
#                 insights_rir_value.append(float('nan'))
#             else:
#                 insightsjson = insightsresponse.json()
#                 insights_p_value.append(insightsjson['p_value'])
#                 insights_rir_value.append(insightsjson['rir_value'])
#                 insights_updated.append(insightsjson['updated_at'])
#     questions_temp['bloknaam'] = resultassignments[i]["name"]
#     questions_temp['exercise_id'] = exercise_id
#     questions_temp['exercise_name'] = exercise_name
#     questions_temp['question_id'] = question_id
#     questions_temp['question_position'] = question_position
#     questions_temp['p_value'] = insights_p_value
#     questions_temp['rir_value'] = insights_rir_value
#     questions_temp['insights_updated'] = insights_updated
#     resultassignments[i]['total points'] = points
#     questions.append(questions_temp)

# %% Resultaat per student
unique_student_id_sorted = list(unique_student_id)
unique_student_id_sorted = []
for i in range(len(unique_student_id)):
    if type(unique_student_id[i]) is int:
        unique_student_id_sorted.append(unique_student_id[i])
unique_student_id_sorted.sort()

Student_number = unique_student_id_sorted[1]

studentresult = {"assignment name":[],"result_id":[],"succeeded":[],"graded":[],"points":[],"mark":[]}
for i in range(len(resultassignments)):
    for j in range(len(resultassignments[i]['student_id'])):
        if resultassignments[i]['student_id'][j] == int(Student_number):
            studentresult["assignment name"].append(resultassignments[i]['name'])
            studentresult["result_id"].append(resultassignments[i]['result_id'][j])
            studentresult["succeeded"].append(resultassignments[i]['succeeded'][j])
            studentresult["graded"].append(resultassignments[i]['graded'][j])
            studentresult["points"].append(resultassignments[i]['points'][j])
            studentresult["mark"].append(resultassignments[i]['mark'][j])
result_student = pd.DataFrame.from_dict(studentresult)

# %% Ruwe resultaten per vraag
import re
import numpy as np

def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    '''
    return [ atoi(c) for c in re.split(r'(\d+)', text) ]

questionsruw = []
date_format = '%Y-%m-%dT%H:%M:%S.%f%z'
#date_obj = datetime.strptime(date_str, date_format)
for i in range(len(resultassignments)):
    questions_ruw = dict()
    questions_ruw["name"] = resultassignments[i]["name"]
    questions_score_temp = []
    gemaakt_temp = []
    nagekeken_temp = []
    studentnummer = []
    question_open = []
    numeral_temp = []
    gestart_op = []
    ingeleverd_op = []
    deelvraag_ingeleverd_temp = []
    for j in tqdm(range(len(resultassignments[i]["result_id"])),desc="Download ruwe result per assignments "+questions_ruw["name"]):
        resultresponse = requests.get("https://ans.app/api/v2/results/"+str(resultassignments[i]["result_id"][j]),headers=header)
        resultjson = resultresponse.json()
        questions_score_temp_temp = []
        nagekeken_temp_temp = []
        gemaakt_temp_temp = []
        numeral_temp_temp = []
        deelvraag_ingeleverd_temp_temp = []
        if resultjson["users"][0]["student_number"] != None :
            studentnummer.append(int(resultjson["users"][0]["student_number"]))
            gestart_op.append(datetime.strptime(resultjson["created_at"],date_format))
            ingeleverd_op.append(datetime.strptime(resultjson["submitted_at"],date_format))
            for k in range(len(resultjson["submissions"])):
                numeral_temp_temp.append(resultjson["submissions"][k]["numeral"])
                if resultjson["submissions"][k]["raw_score"] == None and resultjson["submissions"][k]["score"] == None:
                    nagekeken_temp_temp.append(False)
                else:
                    nagekeken_temp_temp.append(True)
                if resultjson["submissions"][k]["none_above"]: #niet gemaakt
                    gemaakt_temp_temp.append(False)
                    questions_score_temp_temp.append(float('nan'))
                else:
                    gemaakt_temp_temp.append(True)
                    if resultjson["submissions"][k]["score"] != None:
                        questions_score_temp_temp.append(float(resultjson["submissions"][k]["score"]))
                    else:
                        questions_score_temp_temp.append(float('nan'))
                deelvraag_ingeleverd_temp_temp.append(datetime.strptime(resultjson["submissions"][k]["updated_at"],date_format))
                #if question_open[k] == 1 and (resultjson["submissions"][k]["score"] != None and resultjson["submissions"][k]["score"] != "0.0") and resultjson["submissions"][k]["response"] == None:
                #    gemaakt_temp_temp[-1] = False
                #    questions_score_temp_temp[-1] = False
                #    print("Fraude door ",resultjson["users"][0]["student_number"]," bij ",resultassignments[i]["name"]," vraag ",resultjson["submissions"][k]["numeral"])
            numeral_temp_temp_sort = numeral_temp_temp.copy()
            numeral_temp_temp_sort.sort(key = natural_keys)
            idx = []
            for k in numeral_temp_temp_sort:
                idx.append(numeral_temp_temp.index(k))
            questions_score_temp_temp =  list(np.array(questions_score_temp_temp)[np.array(idx)])
            nagekeken_temp_temp = list(np.array(nagekeken_temp_temp)[np.array(idx)])
            gemaakt_temp_temp = list(np.array(gemaakt_temp_temp)[np.array(idx)])
            deelvraag_ingeleverd_temp_temp =  list(np.array(deelvraag_ingeleverd_temp_temp)[np.array(idx)])
            questions_score_temp.append(questions_score_temp_temp)
            nagekeken_temp.append(nagekeken_temp_temp)
            gemaakt_temp.append(gemaakt_temp_temp)
            deelvraag_ingeleverd_temp.append(deelvraag_ingeleverd_temp_temp)
    questions_score_temp = [list(x) for x in zip(*questions_score_temp)] #transpose of list
    nagekeken_temp = [list(x) for x in zip(*nagekeken_temp)]
    gemaakt_temp = [list(x) for x in zip(*gemaakt_temp)]
    deelvraag_ingeleverd_temp = [list(x) for x in zip(*deelvraag_ingeleverd_temp)]
    numeral_temp_temp.sort(key=natural_keys)
    questions_ruw['Deelvraag'] = numeral_temp_temp
    questions_ruw['scores'] = questions_score_temp
    questions_ruw['nagekeken'] = nagekeken_temp
    questions_ruw['gemaakt'] = gemaakt_temp
    questions_ruw['studentennummer'] = studentnummer
    questions_ruw['startdatum'] = gestart_op
    questions_ruw['inleverdatum vraag'] = deelvraag_ingeleverd_temp
    questions_ruw['inleverdatum blok'] = ingeleverd_op
    questionsruw.append(questions_ruw)

# %% analyse results

for i in range(len(questionsruw)):
    score100 =[]
    score0 = []
    score05 = []
    nietgemaakt = []
    nietnagekeken = []
    for j in range(len(questionsruw[i]['scores'])):
        score100.append(questionsruw[i]['scores'][j].count(1))
        score0.append(questionsruw[i]['scores'][j].count(0))
        score05.append(sum(i < 1 for i in questionsruw[i]['scores'][j]) - questionsruw[i]['scores'][j].count(0))
        nietgemaakt.append(questionsruw[i]['gemaakt'][j].count(False))
        nietnagekeken.append(questionsruw[i]['nagekeken'][j].count(False))
    questionsruw[i]['# 100% goed'] = score100
    questionsruw[i]['# 0% goed'] = score0
    questionsruw[i]['# 0 - 100% goed'] = score05
    questionsruw[i]['# niet gemaakt'] = nietgemaakt
    questionsruw[i]['# niet nagekeken'] = nietnagekeken

# %% select blok
Kies_blok_voor_vragen=resultassignments[1]["name"]
for i in range(len(name)):
    if Kies_blok_voor_vragen == resultassignments[i]["name"]:
        index_blok = i
        break

# %% Resultaten per blok
blokresult = resultassignments[index_blok]
result_blok = pd.DataFrame.from_dict(blokresult)
result_blok = result_blok.drop(columns=["assignments_id","name"])

# %% results blok from insights
# result_vragen = pd.DataFrame(list(zip(questions[index_blok]['question_position'],questions[index_blok]['p_value'],questions[index_blok]['rir_value'],questions[index_blok]['insights_updated'])),columns=["Vraag","p-value","rir-value","Updated at"])

# %% exportfile voor Brightspace
import numpy as np
import os
rows = [['OrgDefinedId',resultassignments[index_blok]["name"]+" Points Grade <Numeric MaxPoints:10 Category:COZ>","End-of-Line Indicator"]]
   
    
for i in range(len(resultassignments[index_blok]["calculated_grade"])):
    rows.append(["#"+str(resultassignments[index_blok]["student_id"][i]),resultassignments[index_blok]["calculated_grade"][i],"#"])
 
np.savetxt(os.path.dirname(os.path.realpath(__file__))+"\\"+"Brightspace_output "+resultassignments[index_blok]["name"][0:6]+" "+datetime.now().strftime("%d-%m-%Y_%H-%M-%S")+".csv",
        rows,
        delimiter =",",
        fmt ='% s')
    
# %% plot
import matplotlib.pylab as plt
import numpy as np

# data from https://allisonhorst.github.io/palmerpenguins/

deelvraag = questionsruw[index_blok]["Deelvraag"]
resultaten = dict((k, questionsruw[index_blok][k]) for k in ('# 100% goed', '# 0 - 100% goed', '# 0% goed','# niet nagekeken','# niet gemaakt'))
color = ('green','yellow','red','grey','black')
i = 0
width = 0.75

fig, ax = plt.subplots(figsize=(10, 5))
bottom = np.zeros(len(questionsruw[index_blok]["Deelvraag"]))

   
for boolean, resultaten_i in resultaten.items():
    p = ax.bar(deelvraag, resultaten_i, width, label=boolean, bottom=bottom, color = color[i])
    i+=1
    bottom += resultaten_i

ax.set_title("Scores " + questionsruw[index_blok]["name"])
box = ax.get_position()
ax.set_position([box.x0, box.y0 + box.height * 0.1,
                 box.width, box.height * 0.9])
from matplotlib.ticker import MaxNLocator
ax.yaxis.set_major_locator(MaxNLocator(integer=True))

ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1),
          fancybox=True, shadow=True, ncol=3)

plt.show()

# %% plot activity
fig = plt.figure()
ax = fig.add_subplot(111)
data = []
for i in range(len(questionsruw[index_blok]['inleverdatum vraag'])):
    for j in range(len(questionsruw[index_blok]['inleverdatum vraag'][i])):
        data.append(questionsruw[index_blok]['inleverdatum vraag'][i][j])
ax.hist(data,bins=50,label=questionsruw[index_blok])
ax.set_title("Activiteit " + questionsruw[index_blok]["name"])
import matplotlib.dates as mdates
ax.xaxis.set_minor_locator(mdates.DayLocator(interval=1))
ax.xaxis.set_major_locator(mdates.DayLocator(interval=7))
local_tz=pytz.timezone('Europe/Berlin')
ax.xaxis.set_major_formatter(mdates.DateFormatter('Maandag week %W', tz=local_tz))
plt.ylabel('Indiening vraag')
date_format = '%Y-%m-%dT%H:%M:%S.%f%z'
gestart_op.append(datetime.strptime(resultjson["created_at"],date_format))
#plt.xlim([datetime.strptime("2023-09-23T10:00:00.000+02:00",date_format),datetime.strptime("2023-09-25T00:00:00.000+02:00",date_format)])
#ax.xaxis.set_major_formatter(mdates.DateFormatter('%H', tz=local_tz))
#plt.ylim([0,600])
#fig.autofmt_xdate()